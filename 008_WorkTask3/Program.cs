﻿/*Є N клієнтів, яким компанія-виробник має доставити товар. 
 * Скільки існує можливих маршрутів доставки товару з урахуванням того, що товар доставлятиме одна машина? 
 * Використовуючи Visual Studio, створіть проект за шаблоном Console Application. 
 * Напишіть програму, яка розраховуватиме і виводитиме на екран кількість можливих варіантів доставки товару.
 * Для вирішення задачі, використовуйте факторіал N!, що розраховується за допомогою рекурсії. 
 * Поясніть, чому не рекомендується використовувати рекурсію для розрахунку факторіалу. Вкажіть слабкі місця цього підходу.
 * */
using System;
using System.Diagnostics;
using System.Text;

namespace _008_WorkTask3
{
    class Program
    {
        static int Factorial(int n)
        {
            if (n == 0)
                return 1;
            else
                return n * Factorial(n - 1);
        }

        static void Main()
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            int N = 10;  //кількість клієнтів
                        
            int quantityRoots = Factorial(N); // кількість можливих варіантів доставки товару

            Console.WriteLine($"Кількість можливих варіантів доставки товару = {quantityRoots}");
            Console.WriteLine("Якщо в рекурсії не передбачити виходи із методу, тоді даний метод буде виконуватися, поки не заповнить операційну память");
            Console.ReadKey();
        }
    }
}