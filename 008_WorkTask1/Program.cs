﻿/*Використовуючи Visual Studio, створіть проект за шаблоном Console Application. 
 * Уявіть, що ви реалізуєте програму для банку, яка допомагає визначити, чи погасив клієнт кредит чи ні. 
 * Припустимо, щомісячна сума платежу має становити 100 грн. Клієнт повинен виконати 7 платежів, але може платити рідше за великі суми. 
 * Тобто може двома платежами по 300 і 400 грн. закрити весь обов'язок. 
 * Створіть метод, який як аргумент прийматиме суму платежу, введену економістом банку. 
 * Метод виводить на екран інформацію про стан кредиту (сума заборгованості, сума переплати, повідомлення про відсутність боргу). 
*/
using System;
using System.Diagnostics;
using System.Text;

namespace _008_WorkTask2
{
    class Program
    {
        static string StatusDebts(decimal treatyDebt, int termDebt, decimal SumBills)
        {
            decimal sumPlanBills = treatyDebt/termDebt;

            decimal restDebt = treatyDebt > SumBills? treatyDebt - SumBills: 0 ;

            decimal overBillDebt = SumBills - sumPlanBills;

            if (overBillDebt >= 0)
                return $"Сума заборгованості ={restDebt}, сума переплати={overBillDebt}, борг клієнта в цьому періоді відстутній";
            else return $"Сума заборгованості ={restDebt}, сума недостачі={Math.Abs(overBillDebt)}, є борг клієнта в цьому періоді";

        }

        //перевірка методу
        static void Main()
        {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            decimal treatyDebtClient = 700;
            int termDebtClient = 7;

            Console.Write("прошу ввести суму платежу клієнта=");
            string inputSumClientBills = Console.ReadLine();

            inputSumClientBills = (decimal.TryParse(inputSumClientBills, out decimal SumClientBills)) ? inputSumClientBills : null;
            
            if (inputSumClientBills != null )
            {
                Console.WriteLine($"{StatusDebts(treatyDebtClient, termDebtClient, SumClientBills)}");
}
            else
                Console.WriteLine("Сума платежу введена некоректно");

            Console.ReadKey();
        }
    }
}


